# By Jonah Bobilin, 12 Jan 2020, EE 205
# makefile for Lab 1d

CC = gcc
CFLAGS = -g -Wall

TARGET = summation

# target to create all executables for this lab
all: $(TARGET)

# compile
summation: summation.c
	$(CC) $(CFLAGS) -o $(TARGET) summation.c   

# source file dependencies

# utility targets
clean:
	rm  -f *.o
    
real_clean: clean
	rm  -f trig a.out core

